import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PushPopPage } from './push-pop';

@NgModule({
  declarations: [
    PushPopPage,
  ],
  imports: [
    IonicPageModule.forChild(PushPopPage),
  ],
})
export class PushPopPageModule {}
